import { DynamoDBClient, QueryCommand, DeleteItemCommand, PutItemCommand } from "@aws-sdk/client-dynamodb";
import { S3Client, DeleteObjectCommand } from "@aws-sdk/client-s3";
import { APIGatewayProxyEvent } from "aws-lambda";

const REGION_NAME = process.env.REGION_NAME as string;
const DYNAMO_DB_TABLE_NAME = process.env.DYNAMO_DB_TABLE_NAME as string;
const BUCKET_NAME = process.env.BUCKET_NAME as string;

const dynamoDbClient = new DynamoDBClient({ region: REGION_NAME });
const s3Client = new S3Client({ region: REGION_NAME });

interface GalleryInterface {
    GalleryName: string
}

const putGallery = async (gallery: GalleryInterface) => {
    const { Items } = await dynamoDbClient.send(new QueryCommand({
        TableName : DYNAMO_DB_TABLE_NAME,
        KeyConditionExpression: 'GalleryName = :name',
        ExpressionAttributeValues: {
            ":name": { S: gallery.GalleryName }
        }
    }));

    if(Items?.length !== 0) {
        throw 'Name already exists';
    }

    await dynamoDbClient.send(new PutItemCommand({
        TableName : DYNAMO_DB_TABLE_NAME,
        Item: {
            GalleryName: { S: gallery.GalleryName },
            Images: { L: [] }
        }
    }));
};

const deleteGallery = async (name: string) => {
    const { Items } = await dynamoDbClient.send(new QueryCommand({
        TableName : DYNAMO_DB_TABLE_NAME,
        KeyConditionExpression: 'GalleryName = :name',
        ExpressionAttributeValues: {
            ":name": {S: name}
        }
    }));

    if(Items && Items?.length > 0) {

        await dynamoDbClient.send(new DeleteItemCommand({
            TableName : DYNAMO_DB_TABLE_NAME,
            Key: {
                GalleryName: { S: name }
            },
        }));

        const promises = Items[0].Images.L?.map((image: any) => {
            return {
                Key: image.M?.Key.S,
                Name: image.M?.Name.S
            }
        }).map(image => {
            return s3Client.send(new DeleteObjectCommand({
                Bucket: BUCKET_NAME,
                Key: image.Key
            }));
        });

        await Promise.all(promises || []);
    }
};

export const lambdaHandler = async (event: APIGatewayProxyEvent, context: unknown) => {
    try {
        const method = event.httpMethod
        const name = event.pathParameters?.name

        if (method === 'PUT' && event.body){
            await putGallery(JSON.parse(event.body));

            return {
                statusCode: 200,
                headers: {
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Credentials': true,
                    'Access-Control-Allow-Methods': 'OPTIONS,PUT,DELETE,GET'
                },
                body: JSON.stringify({
                    'status': 'ok'
                })
            }
        } else if (method === 'DELETE' && name) {
            await deleteGallery(name.replace('-', ' ').replace('_', ' '));

            return {
                statusCode: 200,
                headers: {
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Credentials': true,
                    'Access-Control-Allow-Methods': 'OPTIONS,PUT,DELETE,GET'
                },
                body: JSON.stringify({
                    'status': 'ok'
                })
            }
        } else {
            return {
                statusCode: 404,
                headers: {
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Credentials': true,
                    'Access-Control-Allow-Methods': 'OPTIONS,PUT,DELETE,GET'
                },
                body: JSON.stringify({
                    'status': 'not ok'
                })
            }
        }

    } catch (err) {
        console.log(err);
        return {
            statusCode: 500,
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
                'Access-Control-Allow-Methods': 'OPTIONS,PUT,DELETE,GET'
            },
            body: JSON.stringify({
                    err
            })
        }
    }
};
