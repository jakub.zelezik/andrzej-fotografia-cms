import {
    CodePipelineClient,
    GetPipelineExecutionCommand,
    StartPipelineExecutionCommand
} from "@aws-sdk/client-codepipeline";
import { APIGatewayProxyEvent } from "aws-lambda";

const REGION_NAME = process.env.REGION_NAME as string;
const PIPELINE_NAME = process.env.PIPELINE_NAME as string;

const codePipelineClient = new CodePipelineClient({ region: REGION_NAME });

const commit = async () => {
    const command = new StartPipelineExecutionCommand({
        name: PIPELINE_NAME
    });

    const { pipelineExecutionId } = await codePipelineClient.send(command);

    return pipelineExecutionId;
};

const getStatus = async (id: string) => {
    const command = new GetPipelineExecutionCommand({
        pipelineName: PIPELINE_NAME,
        pipelineExecutionId: id
    })

    const { pipelineExecution } = await codePipelineClient.send(command);

    return pipelineExecution?.status
};

export const lambdaHandler = async (event: APIGatewayProxyEvent, context: unknown) => {
    try {
        const method = event.httpMethod;
        const id = event.pathParameters?.id;

        if (method === 'POST'){
            return {
                statusCode: 200,
                headers: {
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Credentials': true,
                    'Access-Control-Allow-Methods': 'OPTIONS,PUT,DELETE,GET'
                },
                body: JSON.stringify({
                    status: 'ok',
                    pipelineExecutionId: await commit()
                })
            }
        } else if (method === 'GET' && id) {

            return {
                statusCode: 200,
                headers: {
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Credentials': true,
                    'Access-Control-Allow-Methods': 'OPTIONS,PUT,DELETE,GET'
                },
                body: JSON.stringify({
                    status: 'ok',
                    pipelineStatus: await getStatus(id)
                })
            }
        } else {
            return {
                statusCode: 404,
                headers: {
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Credentials': true,
                    'Access-Control-Allow-Methods': 'OPTIONS,PUT,DELETE,GET'
                },
                body: JSON.stringify({
                    status: 'not ok'
                })
            }
        }

    } catch (err) {
        console.log(err);
        return {
            statusCode: 500,
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
                'Access-Control-Allow-Methods': 'OPTIONS,PUT,DELETE,GET'
            },
            body: JSON.stringify({
                    err
            })
        }
    }
};
