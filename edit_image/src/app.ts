import { DynamoDBClient, QueryCommand, UpdateItemCommand } from "@aws-sdk/client-dynamodb";
import { S3Client, DeleteObjectCommand, PutObjectCommand } from "@aws-sdk/client-s3";
import { APIGatewayProxyEvent } from "aws-lambda";
import * as uuid from "uuid";

const REGION_NAME = process.env.REGION_NAME as string;
const DYNAMO_DB_TABLE_NAME = process.env.DYNAMO_DB_TABLE_NAME as string;
const BUCKET_NAME = process.env.BUCKET_NAME as string;

const dynamoDbClient = new DynamoDBClient({ region: REGION_NAME });
const s3Client = new S3Client({ region: REGION_NAME });


const addImageToGallery = async ({ galleryName, image, mimeType }: { galleryName: string, image: Buffer; mimeType: string }) => {
    const key = uuid.v4();

    await s3Client.send(new PutObjectCommand({
        Bucket: BUCKET_NAME,
        Key: key,
        Body: image,
        ContentType: mimeType
    }));

    await dynamoDbClient.send(new UpdateItemCommand({
        TableName: DYNAMO_DB_TABLE_NAME,
        Key: { GalleryName: { S: galleryName } },
        UpdateExpression: "SET Images = list_append(Images, :i)",
        ExpressionAttributeValues: {
            ':i': {L: [{M: { Name: {S: key}, Key: {S: key}}}]},
        }
    }));

    return key;
}

const deleteImageFromGallery = async ({ galleryName, key }: { galleryName: string; key: string }) => {
    const { Items } = await dynamoDbClient.send(new QueryCommand({
        TableName : DYNAMO_DB_TABLE_NAME,
        KeyConditionExpression: 'GalleryName = :name',
        ExpressionAttributeValues: {
            ":name": { S: galleryName }
        }
    }));

    if(Items && Items?.length > 0) {
        const index = Items[0].Images.L?.map((image: any) => {
            return {
                Key: image.M?.Key.S,
                Name: image.M?.Name.S
            }
        }).findIndex(image => image.Key === key);

        if(index !== -1) {
            await dynamoDbClient.send(new UpdateItemCommand({
                TableName: DYNAMO_DB_TABLE_NAME,
                Key: { GalleryName: { S: galleryName } },
                UpdateExpression: `REMOVE Images[${index}]`,
            }));

            return s3Client.send(new DeleteObjectCommand({
                Bucket: BUCKET_NAME,
                Key: key
            }));
        }
    }
}

export const lambdaHandler = async (event: APIGatewayProxyEvent, context: unknown) => {
    try {
        const method = event.httpMethod
        const galleryName = event.pathParameters?.name;
        const key = event.pathParameters?.key;

        if (method === 'PUT' && event.body && galleryName) {
            const body = JSON.parse(event.body);

            return {
                statusCode: 200,
                headers: {
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Credentials': true,
                    'Access-Control-Allow-Methods': 'OPTIONS,PUT,DELETE,GET'
                },
                body: JSON.stringify({
                    'status': 'ok',
                    'key': await addImageToGallery({
                        image: new Buffer(body.image, 'base64'),
                        mimeType: body.mimeType,
                        galleryName: galleryName?.replace('-', ' ').replace('_', ' ')
                    })
                })
            };
        } else if (method === 'DELETE' && galleryName && key) {
            await deleteImageFromGallery({
                galleryName: galleryName?.replace('-', ' ').replace('_', ' '),
                key
            })

            return {
                statusCode: 200,
                headers: {
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Credentials': true,
                    'Access-Control-Allow-Methods': 'OPTIONS,PUT,DELETE,GET'
                },
                body: JSON.stringify({
                    'status': 'ok'
                })
            }
        } else {
            throw Error("ups");
        }

    } catch (err) {
        console.log(err);
        return {
            statusCode: 403,
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
                'Access-Control-Allow-Methods': 'OPTIONS,PUT,DELETE,GET'
            },
            body: JSON.stringify({
                err
            })
        };
    }
};
