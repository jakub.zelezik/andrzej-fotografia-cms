import { DynamoDBClient, QueryCommand, ScanCommand } from "@aws-sdk/client-dynamodb";
import { S3Client, GetObjectCommand } from "@aws-sdk/client-s3";
import { APIGatewayProxyEvent } from "aws-lambda";
import { getSignedUrl } from "@aws-sdk/s3-request-presigner";

const REGION_NAME = process.env.REGION_NAME as string;
const DYNAMO_DB_TABLE_NAME = process.env.DYNAMO_DB_TABLE_NAME as string;
const BUCKET_NAME = process.env.BUCKET_NAME as string;

const dynamoDbClient = new DynamoDBClient({ region: REGION_NAME });
const s3Client = new S3Client({ region: REGION_NAME });

const listGalleryContent = async (name: string) => {
    const result = await dynamoDbClient.send(new QueryCommand({
        TableName : DYNAMO_DB_TABLE_NAME,
        KeyConditionExpression: 'GalleryName = :name',
        ExpressionAttributeValues: {
            ":name": {S: name}
        }
    }));

    if(result.Items?.length) {
        const ImagesPromise = result.Items[0].Images.L?.map(image => { return {
            Key: image.M?.Key.S as string,
            Name: image.M?.Name.S as string,
        }}).map(async (image) => {
            const command = new GetObjectCommand({
                Bucket: BUCKET_NAME,
                Key: image.Key,
            });

            return {
                Key: image.Key,
                Name: image.Name,
                Url: await getSignedUrl(s3Client, command, { expiresIn: 3600 })
            };
        });

        const Images = await Promise.all(ImagesPromise || []);

        return {
            statusCode: 200,
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
                'Access-Control-Allow-Methods': 'OPTIONS,PUT,DELETE,GET'
            },
            body: JSON.stringify({
                gallery: {
                    GalleryName: result.Items[0].GalleryName.S,
                    Images
                }
            })
        }
    } else {
        throw Error('invalid name');
    }
};


const listGalleries = async () => {
    const result = await dynamoDbClient.send(new ScanCommand({
        TableName : DYNAMO_DB_TABLE_NAME
    }));

    return {
        statusCode: 200,
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': true,
            'Access-Control-Allow-Methods': 'OPTIONS,PUT,DELETE,GET'
        },
        body: JSON.stringify({
            galleries: await Promise.all(result.Items?.map(async (gallery) => {
                return {
                    GalleryName: gallery.GalleryName.S,
                    Image: gallery.Images.L?.length ? {
                        Key: gallery.Images.L[0].M?.Key.S,
                        Name: gallery.Images.L[0].M?.Name.S,
                        Url: await getSignedUrl(s3Client, new GetObjectCommand({
                            Bucket: BUCKET_NAME,
                            Key: gallery.Images.L[0].M?.Key.S,
                        }), { expiresIn: 3600 })
                    } : null,
                }
            }) || [])
        })
    }
}


export const lambdaHandler = async function (event: APIGatewayProxyEvent, context: unknown) {
    try {
        const name = event.pathParameters?.name;

        if(name) {
            return await listGalleryContent(name.replace('-', ' ').replace('_', ' '));
        } else {
            return await listGalleries();
        }
    } catch (err) {
        console.log(err);
        return err;
    }
};
